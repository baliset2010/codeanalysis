#!/bin/sh

NKF=`which nkf`
if [ "${NKF}" = "" ]; then
	echo requires nkf
	exit 1
fi

for EXT in s c h
do
	# どんな種類のファイルがいるのか？
	#LINES=`find . -name "*.${EXT}" -exec nkf -g {} /dev/null \; | grep -v Shift_JIS | grep -v ASC | grep -v CP932`

	FILES=`find . -name "*.${EXT}" -exec nkf -g {} /dev/null \; | grep -e Shift_JIS -e CP932 | awk -F':' '{print $1}'`
	for FILE in ${FILES}
	do
		echo ${FILE}
		nkf -w -d --overwrite ${FILE}
	done
done

