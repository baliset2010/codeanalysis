#!/bin/sh

NKF=`which nkf`
if [ "${NKF}" = "" ]; then
	echo requires nkf
	exit 1
fi

for EXT in s c h
do
	FILES=`find . -name "*.${EXT}" -exec nkf -g {} /dev/null \; | grep UTF-8 | awk -F':' '{print $1}'`
	for FILE in ${FILES}
	do
		echo ${FILE}
		nkf -s -d --overwrite ${FILE}
	done
done

