#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>

#define	F(X,Y,Z) \
	void X(void) { \
		while(true) { \
			if ((random() % 100) < Z) { \
				Y(); \
			} \
			if ((random() % 100) < Z) { \
				return; \
			} \
			sleep(1); \
		} \
	}

void level_010(void) {
	while(true) {
		if ((random() % 100) < 2) {
			return;
		}
		sleep(1);
	}
}

F(level_009, level_010, 5)
F(level_008, level_009, 5)
F(level_007, level_008, 5)
F(level_006, level_007, 5)
F(level_005, level_006, 5)
F(level_004, level_005, 5)
F(level_003, level_004, 5)
F(level_002, level_003, 5)

void level_001(void) {
	while(true) {
		if ((random() % 100) < 5) {
			level_002();
		}
		if ((random() % 100) < 5) {
			return;
		}
	}
}

void *thread(void *arg) {

	while(true) {
		if ((random() % 100) < 5) {
			level_001();
		}
		sleep(1);
	}
	return(NULL);
}

int main(int argc, char *argv[]) {
	pthread_t t1, t2, t3;
	extern void PrintFuncStack(FILE *fp);

	pthread_create(&t1, NULL, thread, NULL);
	pthread_create(&t2, NULL, thread, NULL);
	pthread_create(&t3, NULL, thread, NULL);

	while(true) {
		sleep(10);
		PrintFuncStack(stdout);
	}

	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
	pthread_join(t3, NULL);
	return(0);
}

