#!/bin/sh

USAGE=
if [ "$1" = "" ]; then
	USAGE="Y"
fi
if [ "$2" = "" ]; then
	USAGE="Y"
fi
if [ "$3" = "" ]; then
	USAGE="Y"
fi

if [ "${USAGE}" = "Y" ]; then
	echo "Usage: $0 EXEC_FILE_NAME LOG_FILE OUT_FILE"
	exit 1
fi

EXEC=$1
LOG=$2
OUT=$3
ADDR2LINE=addr2line

FLAG="Y"
cp ${LOG} ${OUT}

while [ "${FLAG}" = "Y" ]; do
	LINE=`grep "@0x" ${OUT} | head -1`
	if [ "${LINE}" = "" ]; then
		FLAG="N"
	else
		ADDR=`expr "${LINE}" : ".*@\(0x.[0123456789abcdefABCDEF]*\).*"`
		SYMBOL=`${ADDR2LINE} -s -e ${EXEC} ${ADDR} | head -1`
		echo "${ADDR} ==> ${SYMBOL}"
		#sed -i -e "s/@${ADDR}/${SYMBOL}/g" ${OUT}
		sed -i -e "s%@${ADDR}%${SYMBOL}%g" ${OUT}
	fi
done

