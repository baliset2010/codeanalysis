#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <malloc.h>

#ifdef __linux__
#define	_GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#define	__USE_GNU
#include <dlfcn.h>
#endif

void __cyg_profile_func_enter (void *func, void *caller)
   	__attribute__((no_instrument_function));
void __cyg_profile_func_exit  (void *func, void *caller)
   	__attribute__((no_instrument_function));

#define	cMaxFunctionStack	100

typedef struct stSlot {
#ifdef __linux__
	unsigned int thread;
#else
	pthread_t thread;
#endif
	int deepest;
	int level;
	void *funcs[cMaxFunctionStack];
#ifdef __linux__
	const char *names[cMaxFunctionStack];
#endif
	void *callers[cMaxFunctionStack];
	struct stSlot *nextSlot;
} sSlot;

static sSlot *gHead = NULL;
static sSlot *gTail = NULL;

void __cyg_profile_func_enter (void *func, void *caller) {
#ifdef __linux__
	unsigned int thread;
	Dl_info dli;
#else
	pthread_t thread;
#endif
	sSlot *slot;
	bool needInit = false;
	const char *name;

#ifdef __linux__
	thread = syscall(SYS_gettid);
#else
   	thread = pthread_self();
#endif
	if (gHead == NULL) {
		/* create first thread slot */
		gHead = malloc(sizeof(sSlot));
		if (gHead == NULL)	return;
		gTail = gHead;
		slot = gHead;
		needInit = true;
		printf("create first slot\n");
	} else {
		/* follow slot chain to find the correct thread slot */
		slot = gHead;
		while(slot != NULL) {
			if (slot->thread == thread) {
				break;
			}
			slot = slot->nextSlot;
		}
		if (slot == NULL) {
			/* cannot find the thread, so create one */
			slot = malloc(sizeof(sSlot));
			if (slot == NULL) return;
			gTail->nextSlot = slot;
			gTail = slot;
			needInit = true;
			printf("create new slot\n");
		}
	}

	if (needInit) {	/* since it was allocated this time */
		slot->thread = thread;
		slot->deepest = -1;
		slot->level = -1;
		slot->nextSlot = NULL;
	}

	(slot->level)++;
	if (slot->deepest < slot->level) {
		slot->deepest = slot->level;
	}

	if (slot->level >= cMaxFunctionStack) {
		/* give up logging */
		printf("overflow slot\n");
		return;
	}

	slot->funcs[slot->level] = func;
	slot->callers[slot->level] = caller;
#ifdef __linux__
	if (dladdr(func, &dli) == 0) {
		name = NULL;
	} else {
		name = dli.dli_sname;
	}
	slot->names[slot->level] = name;
#endif
}

void __cyg_profile_func_exit  (void *func, void *caller) {
#ifdef __linux__
	unsigned int thread;
#else
	pthread_t thread;
#endif
	sSlot *slot;

#ifdef __linux__
	thread = syscall(SYS_gettid);
#else
	thread = pthread_self();
#endif

	slot = gHead;
	while(slot != NULL) {
		if (slot->thread == thread) {
			(slot->level)--;
			return;
		}
		slot = slot->nextSlot;
	}
	printf("thread not found at exit:%p\n", func);
}

__attribute__((no_instrument_function))
void PrintFuncStack(FILE *fp) {
	sSlot *slot;
	int i, n;

	if (gHead == NULL) {
		fprintf(fp, "no func stack\n");
		return;
	}

	slot = gHead;
	n = 1;
	while(slot != NULL) {
#ifdef __linux__
		fprintf(fp, "%d.\tthread:%u\n", n, slot->thread);
#else
		fprintf(fp, "thread %d\n", n);
#endif
		fprintf(fp, "\tdeepest:%d\n", slot->deepest);
		fprintf(fp, "\tlevel:%d\n", slot->level);
		for(i=0; i<=slot->level; i++) {
			fprintf(fp, "\t\t@%p\t", slot->funcs[i]);
#ifdef __linux__
			if (slot->names[i] != NULL) {
				fprintf(fp, "%s\t", slot->names[i]);
			} else {
				fprintf(fp, "?\t");
			}
#endif
			fprintf(fp, "@%p\n", slot->callers[i]);
		}
		n++;
		slot = slot->nextSlot;
	}
}

