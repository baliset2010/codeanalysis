#!/bin/sh

gcc -Wall -fPIC -c funcstack.c 
ar rcs libfuncstack.a funcstack.o 

gcc -Wall -fPIC -finstrument-functions -o test-funcstack test-funcstack.c libfuncstack.a -ldl -rdynamic -lpthread
