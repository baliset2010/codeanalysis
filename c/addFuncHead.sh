#!/bin/sh
#
# 標準入力から受け取った関数名をディレクトリ配下のCファイルより探し出して、そこにトレース用の
# マクロを埋め込む

CTAGS=`which ctags`
if [ "${CTAGS}" = "" ]; then
	echo "requires 'ctags'"
	exit 1
fi
AWK=`which awk`
if [ "${AWK}" = "" ]; then
	echo "requires 'awk'"
	exit 1
fi

# 可変パラメータ
FUNC="TRACE"					# マクロ名
REMOVE="N"						# マクロ削除
MARK="addFuncHeadMark"			# マクロのマーク（印)
INCLUDE="debug_trace.h"			# インクルード・ファイル名
LIST_FILE=""					# 関数リスト・ファイル

while getopts "rf:m:i:l:" flag; do
	case $flag in
	  \?) OPT_ERROR=1; break;;
	  r) REMOVE="Y";;
	  f) FUNC="$OPTARG";;
	  m) MARK="$OPTARG";;
	  i) INCLUDE="$OPTARG";;
	  l) LIST_FILE="$OPTARG";;
	esac
done

shift $(( $OPTIND - 1 ))

if [ $OPT_ERROR ]; then
	echo >&2 "usage: $0 [-r] [-f function] [-m mark] [-l function_list]"
	echo >&2 "  -r           remove marked line"
	echo >&2 "  -f function  add function at the head of functions"
	echo >&2 "  -m mark      add mark as comment for added line, to later removable"
	echo >&2 "  -i include   include file name"
	echo >&2 "  -l function_list  function list in a file"
	echo >&2 " "
	echo >&2 "included file(debug_trace.h) should contains,..."
	echo >&2 "static int debug_trace(char *file, char *func) {"
	echo >&2 "printf(\"%s:%s\\n\", file, func);"
	echo >&2 "return(1);"
	echo >&2 "}"
	echo >&2 "#define TRACE int unsed_variable=debug_trace(__FILE__, __func__);"
	exit 1
fi

# 一旦、全ての見つかったマークの行を無条件に削除
FILES=`find . -name "*.c"`
for FILE in ${FILES}
do
	sed -i -e "s/{${FUNC}; \/\*${MARK}\*\//{/" ${FILE}
	sed -i -e "/#include.*\/\*${MARK}\*\//d" ${FILE}
done

# 処理が「削除」なら、これで終了
if [ "${REMOVE}" = "Y" ]; then
	# remove only
	exit 0
fi

# ctags からファル名、行番号、関数名を抽出
LISTS=`${CTAGS} -R -x -f - | ${AWK} '{ if ($2=="function") printf("%s:%d:%s\n", $4,$3,$1);}'`

if [ "${LIST_FILE}" = "" ]; then
	# 無条件にマクロを追加
	for LIST in ${LISTS}
	do
		FILE=`expr "${LIST}" : "\(.*\):.*:.*"`
		LNO=`expr "${LIST}" : ".*:\(.*\):.*"`
		FUN=`expr "${LIST}" : ".*:.*:\(.*\)"`
		NO=`expr ${LNO} + 1`

		FNAME=$(basename "${FILE}")
		EXT="${FNAME##*.}"
		if [ "${EXT}" = "c" ]; then
			L=`sed -n ${NO}p ${FILE}`
			###echo "${L}"
			if [ "${L}" = "{" ]; then
				###echo "Found ${FC} at ${FILE} ${LNO}"
				sed -i -e "${NO}s/{/{${FUNC}; \/\*${MARK}\*\//" ${FILE}
				FOUND="Y"
			fi
		fi
	done
else
	# 関数名が一致した場合のみマクロを追加
	while read FC
	do
		if [ ! "${FC}" = "" ]; then
			FOUND="N"
			for LIST in ${LISTS}
			do
				FILE=`expr "${LIST}" : "\(.*\):.*:.*"`
				LNO=`expr "${LIST}" : ".*:\(.*\):.*"`
				FUN=`expr "${LIST}" : ".*:.*:\(.*\)"`
				NO=`expr ${LNO} + 1`

				if [ "${FUN}" = "${FC}" ]; then
					FNAME=$(basename "${FILE}")
					EXT="${FNAME##*.}"
					if [ "${EXT}" = "c" ]; then
						L=`sed -n ${NO}p ${FILE}`
						###echo "${L}"
						if [ "${L}" = "{" ]; then
							###echo "Found ${FC} at ${FILE} ${LNO}"
							sed -i -e "${NO}s/{/{${FUNC}; \/\*${MARK}\*\//" ${FILE}
							FOUND="Y"
							break
						fi
					fi
				fi
			done

			if [ "${FOUND}" = "N" ]; then
				echo "${FC} was not found"
			fi
		fi
	done <${LIST_FILE}
fi


# インクルードファイルを埋め込む
# マークが存在するCファイルのみ追加する
FILES=`find . -name "*.c" -exec grep -l "${MARK}" {} \;`
for FILE in ${FILES}
do
	FNAME=$(basename "${FILE}")
	EXT="${FNAME##*.}"
	if [ "${EXT}" = "c" ]; then
		sed -i "1s/^/#include \"${INCLUDE}\" \/\*${MARK}\*\/\n/" ${FILE}
	fi
done

