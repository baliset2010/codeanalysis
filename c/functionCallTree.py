#!/usr/bin/env python3

"""
#ソース変更解析#

C言語のソースを解析する。

1. ctags を使い関数と、そのファイル、行番号を取得する
2. 上記から、関数名、ファイル名、行番号、関数定義、関数の内容を SQLITEデータベースに格納する
3. 関数の開始を ctags の指定行から '{' が含まれている行の次の行から
4. 関数の終わりは '}' で始まる行まで
5. 関数の内容に関数名が含まれていれば参照されていると判断

できること
1. 関数のコールツリーを出力（ルートの関数名を記述）
2. 呼び出し元が見つからない関数のリスト出力
3. 重要な関数の中にマーク（ユニークな文字列）を記述し、それを含む関数のコールツリーを出力
"""

import sys
import os
import io
import subprocess
import sqlite3

gDebug = False
cMaxFunctionNest = 100
gTab = '\t'

def getOutputFunctionList():
    """
    ctags でカレントディレクトリ以下のCソースの関数リストを作成する。作成した関数リストは、
    
    関数名[タブ]ファイル名[タブ]行番号[タブ]関数宣言[改行]

    の複数行のテキストで返却される。
    """
    try:
        lines = subprocess.check_output(['ctags', '-R', '-x'])
    except:
        print('must have ctags')
        sys.exit(2)

    out = io.StringIO()
    for line in lines.decode().split('\n'):
        items = line.split(' ')
        if len(items) > 3:
            if items[1] == 'function':
                funcName = items[0]
                for i in range(2,len(items)):
                    if items[i] != '':
                        break
                lineNo = int(items[i])
                fileName = items[i+1]
                definition = ' '.join(items[i+2:])
                if gDebug:
                    print('Function Found - ' + funcName + ' at ' + fileName + ':' + str(lineNo))
                out.write(funcName + '\t' + fileName + '\t' + str(lineNo) + '\t' + definition + '\n')
    return out.getvalue()

def getContentOfFunction(fileName, lineNo):
    """
    ファイル名と行番号から、そこから始まる関数の内容を返す
    """
    out = io.StringIO()
    n = 1
    for line in open(fileName, 'r').readlines():
        l = line.strip() 
        if n == lineNo:
            start = False
            end = False
            if l.find('{') >= 0:
                start = True
        elif n > lineNo:
            if end == False:
                if start:
                    out.write(line)
                    if line.startswith('}'):
                        start = False
                        end = True
                else:
                    if l.endswith('{'):
                        start = True
        n = n + 1
    return out.getvalue()

def createFuncionDatabase(cursor, lines):
    """
    ctags の結果から関数データベース(functions)を作成する
    """
    for line in lines.split('\n'):
        items = line.split('\t')
        if len(items) > 3:
            funcName = items[0]
            fileName = items[1]
            lineNo = items[2]
            definition = items[3]
            content = getContentOfFunction(fileName, int(lineNo))
            if gDebug:
                print('Entry Funciton:' + funcName)
            sql = "INSERT INTO function VALUES('" + funcName + "','" + fileName + "','" + lineNo + "','" + definition + "',0,?)"
            try:
                cursor.execute(sql, [content])
            except sqlite3.IntegrityError:
                print('Duplicated Function Found:', funcName)                
    if gDebug:
        c = cursor.execute('select * from function')
        print(str(len(c.fetchall())) + ' functions found')

def functionNamable(chr):
    """
    文字が関数名として有効な文字か、どうかを返す
    """
    if 'abcdefghijklmnopqrstuvwxyz'.find(chr) >= 0:
        return True
    if 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.find(chr) >= 0:
        return True
    if '0123456789_'.find(chr) >= 0:
        return True
    return False

def createFunctionReference(cursor):
    """
    関数データベース(function)から呼び出しデータベース(reference)を作成する
    """
    r = cursor.execute('select func, content from function')
    funcs = [item[0] for item in r]
    #print(funcs)
    r = cursor.execute('select func, content from function')
    for src in r.fetchall():
        func = src[0]
        content = src[1]
        if gDebug:
            print('func:' + func + ' dst:' + str(len(funcs)))
        for dst in funcs:
            #print('\tsrc:' + func + ' dst:' + dst)
            #print('content:' + content)
            dstFunc = dst + '('
            pos = 0
            while(pos >= 0):
                pos = content.find(dstFunc, pos)
                if pos >= 0:
                    if gDebug:
                        print('pos:' + str(pos) + ' src:' + func + ' dst:' + dst)
                    if pos > 0:
                        if functionNamable(content[pos - 1]) == False:
                            try:
                                cursor.execute("INSERT INTO reference VALUES('" + func + dst + "','" + func + "','" + dst + "')")
                            except sqlite3.IntegrityError:
                                pass
                        else:
                            if gDebug:
                                print("ERR:(" + content[pos - 1] + ")")
                    pos = pos + len(dstFunc)


def createDatabase(databaseName, lines):
    """
    ctags の関数に関する情報のテキストラインから、データベースを作成する
    """
    connection = sqlite3.connect(databaseName)

    cursor = connection.cursor()
    cursor.execute('DROP TABLE IF EXISTS function')
    cursor.execute('DROP TABLE IF EXISTS reference')
    cursor.execute('CREATE TABLE IF NOT EXISTS function (func TEXT PRIMARY KEY, file TEXT, lineNo INTEGER, definition TEXT, mark INTEGER, content TEXT)')
    cursor.execute('CREATE TABLE IF NOT EXISTS reference (id TEXT PRIMARY KEY, src TEXT, dst TEXT)')

    createFuncionDatabase(cursor, lines)
    createFunctionReference(cursor)

    connection.commit()
    connection.close()

def _markFlow(cursor, func, mark, nest):
    """
    指定の関数にマークが入っていた場合、そこまでの関数コール・ツリーをデータベース(mark)に
    書き出す。マークが入っていない場合は、コール・ツリーを続ける
    """
    r = cursor.execute("SELECT content FROM function where func='" + func + "'")
    rlist = r.fetchall()
    assert len(rlist) == 1
    for src in rlist:
        if mark in src[0]:
            if gDebug:
                print(func + ' had mark')
            cursor.execute("UPDATE function SET mark=2 WHERE func='" + func + "'")
            for f in nest:
                cursor.execute("UPDATE function SET mark=1 WHERE func='" + f + "' and mark=0")
            return
    r = cursor.execute("SELECT dst FROM reference where src='" + func + "'")
    nest.append(func)
    for dst in r.fetchall():
        if dst[0] in nest:
            return
        _markFlow(cursor, dst[0], mark, nest)
    nest.pop(len(nest) - 1)

def markFunctionDatabase(cursor, mark):
    """
    指定のマークの入った関数コール・ツリーのデータベースの mark 値を更新する
    """
    r = cursor.execute("SELECT func, content FROM function")
    for src in r.fetchall():
        if mark in src[1]:
            if gDebug:
                print(src[0] + ' has mark')
            cursor.execute("UPDATE function SET mark=2 WHERE func='" + src[0] + "'")
            rr = cursor.execute("SELECT dst FROM reference WHERE src='" + src[0] + "'")
            for dst in rr.fetchall():
                _markFlow(cursor, dst[0], mark, [src[0]])

def markFlow(mark):
    """
    特定の文字列が存在する関数に関してのみコール・ツリーをプリントする
    """
    connection = sqlite3.connect(databaseName)
    cursor = connection.cursor()

    markFunctionDatabase(cursor, mark)

    connection.commit()
    connection.close()

def getMarkSign(mark):
    """
    関数のマークの状態を返す
    """
    if mark == 0:
        return ""
    elif mark == 1:
        if gDebug:
            return " (-)"
        return ""
    return " (*)"

def flowFunc(cursor, funcName, nest, markLevel, printed):
    """
    指定の関数の再帰的にプリントする
    """
    # マーク・レベルのより下なら無視する
    r = cursor.execute("SELECT mark FROM function where func='" + funcName + "'")
    rlist = r.fetchall()
    assert len(rlist) == 1
    for src in rlist:
        if src[0] < markLevel:
            return

    ornament = ''
    if funcName in nest:
        ornament = ' RECURSIVE...'
    elif funcName in printed:
        ornament = ' ...'
    print((gTab * len(nest)) + funcName + ornament + getMarkSign(src[0]))
    if ornament != '':
        return

    printed.append(funcName)
    nest.append(funcName)
    r = cursor.execute("SELECT dst FROM reference WHERE src='" + funcName + "'")
    for dst in r.fetchall():
        func = dst[0]
        flowFunc(cursor, func, nest, markLevel, printed)
    nest.pop(len(nest) - 1)

def findRootFunction(databaseName, markLevel):
    """
    呼び出している関数が見つからない関数のリストを返す
    """
    connection = sqlite3.connect(databaseName)
    cursor = connection.cursor()
    r = cursor.execute("SELECT func,mark FROM function")
    ret = []
    for src in r.fetchall():
        func = src[0]
        mark = src[1]
        if mark >= markLevel:
            rr = cursor.execute("SELECT src FROM reference where dst='" + func + "'")
            if len(rr.fetchall()) == 0:
                ret.append(func)
    connection.close()
    return ret

def flowFuncAll(cursor, markLevel, printed):
    """
    呼び出している関数が見つからない関数すべてのコール・ツリーを表示    
    """
    for func in findRootFunction(databaseName, markLevel):
        flowFunc(cursor, func, [], markLevel, printed)

def flowFunction(funcName, markLevel):
    connection = sqlite3.connect(databaseName)
    cursor = connection.cursor()
    printed = []
    if funcName != '':
        flowFunc(cursor, funcName, [], markLevel, printed)
    else:
        flowFuncAll(cursor, markLevel, printed)
    connection.close()

def listTest(lst, level):
    if level > 10:
        print(str(level) + "\t" + str(lst))
        return
    lst.append(str(level))
    print(str(level) + "\t" + str(lst))
    listTest(lst, level + 1)
    print(str(level) + "\t" + str(lst))


if __name__ == '__main__':
    databaseName = 'function.db'
    mark = ''
    funcName = ''
    markLevel = 0
    for i in range(1, len(sys.argv)):
        arg = sys.argv[i]
        if arg.startswith('mark='):
            mark = arg[5:]
            markFlow(mark)
            markLevel = 1
        elif arg.startswith('db='):
            databaseName = arg[3:]
        elif arg.startswith('func='):
            funcName = arg[5:]
        elif arg.startswith('tab='):
            gTab = ' ' * int(arg[4:])
        elif arg == 'create':
            lines = getOutputFunctionList()
            createDatabase(databaseName, lines)
        elif arg == 'flow':
            flowFunction(funcName, markLevel)
        elif arg == 'root':
            for r in findRootFunction(databaseName, markLevel):
                print(r)
        elif arg == 'debug':
            gDebug = True
        elif arg == 'listTest':
            listTest([], 0)
        else:
            print('Usage:', sys.argv[0], ' [options] commands...')
            print('   options: [debug] [db=DATABASE_FILE_NAME] [tab=N] [func=BASE_FUNC_NAME]')
            print('   commands: create/root/mark=XXXX/flow')
            sys.exit(1)

sys.exit(0)
