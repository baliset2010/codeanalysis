#!/bin/sh

# サブディレクトリ以下のファイルと、それに含まれる関数のリストを出力する
# 'ファイル名' 以下の文は、ファイルの説明を取り出す処理。プロジェクトにより変更が必要

FILES=`find . -name "*.c"`

for FILE in ${FILES}; do
	echo ${FILE}
	RET=`grep "ファイル名" ${FILE}`
	if [ $? -eq 0 ]; then
		FNAME=`basename ${FILE}`
		CMNT=`expr "${RET}" : "^.*${FNAME}\(.*\)"`
		CMNT=`echo ${CMNT}`
		echo -e "\t${CMNT}"
	fi

	#ctags -f - ${FILE} | awk -F'\t' '{ if ($4 == "f") print("\t\t", substr($3, 3, length($3) - 6)); }'
	ctags -f - ${FILE} | awk -F'\t' '{ if ($4 == "f") print("\t\t", $1); }'
done

