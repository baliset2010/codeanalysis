# C analysis tools #

1. funcstack - create function calls tree. Requires gcc and run the program.
2. egypt-frieve - create function call using Frieve Editor, requires egypt.
3. addFuncHead.sh - add TRACE MACRO at the head of functions.
4. debug_trace.h - use with addFuncHead.sh
5. showDigest.sh - show digest of the project using 'ctags'. 
6. functionCallTree.py - create function tree.

also use,...

cflow -m FUNCTION_NAME -b --omit-arguments `find . -name "*.c"`

