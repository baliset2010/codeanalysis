# C Language Function Call Graph #

There are several ways to create C function call graph.

I thought using [egypt](http://www.gson.org/egypt/) and [graphviz](http://www.graphviz.org) was a right choice. But sometimes, it is cumbersome to edit the graph.

There is a splendid tool called [Frieve Editor](http://www.frieve.com/feditor/), which is a tool to clarify ideas, draw charts for Windows, AND actually, this tool do MORE!

This shell script converts the outputs of ctags and egypt to input file for Frieve Editor, to edit C function call graph, very easily.

## platform ##

Only tested on Linux. Wine to run Frieve Editor.
You can convert file on Linux, then run Frieve Editor on Windows.
Other Unix(FreeBSD, NetBSD, Mac) may work.

## preparation ##

* ctags
* egypt
* Wine(if you want to run Frieve Editor on Linux)
* Frieve Editor

### ctags ###

just use apt-get or rpm to install ctags.
Only functions which was ctags-ed files, will appear on Frieve Editor. This will avoid too many functions in one Frieve Editor's file.

To create tag file, command should be like,...

```
ctags -x *.c *.h >FUNC.TAG
```

if you only want to functions to display,...

**By default, function/member(struct)/struct/typedef/variable/enumurator will be displayed.**

```
ctags -x *.c | grep " function " >FUNC.TAG
```

### egypt ###

Download [egypt](http://www.gson.org/egypt/).

```
tar zxpf egypt-1.10.tar.gz
cd egypt-1.10
perl Makefile.PL
make
make install
```

To create dot file, type like,...

```
cd YOUR_PROJECT_DIRECTORY_TO_BUILD
make clean
make CFLAGS+=-fdump-rtl-expand
```

you will see there are YOUR_C_SOURCE_FILE.c.XXX.expand's.

```
egypt *.expand >FUNC.DOT
```

### Convert tag and dot files to Frieve Editor's fip ###

```
./ce2f.sh FUNC.TAG FUNC.DOT FUNC.fip [-D] [-F] [-E]
```

* -D : do not delete temporary files
* -F : filter only function
* -E : filter out external function

### Wine ###

just use apt-get or rpm to install Wine.

Frieve Editor doesn't do any tricky things, so it should work with default settings.

### Frieve Editor ###

Download [Frieve Editor]() and unzip.
Run editor.exe with Wine.

If you want to see menus and titles in Japanese, you can change it English as below.

![a.png](https://bitbucket.org/repo/LAz8BE/images/697390231-a.png)

Then quit the program, and restart it. You will see English menus and titles.

## Conversion Detail ##

This script convert files,...

1. read ctags file, find out functions, their function/file names and defined lines.
2. find function in dot file, to search which functions are called.
3. gather information then create Frieve Editor's fip file.
