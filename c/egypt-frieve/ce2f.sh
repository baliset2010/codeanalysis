#!/bin/sh
#
# ctags -x *.h *.c > func.tag
#
# install egypt
# compile with RTL, like,...
# make clean
# make CFLAGS+=-fdump-rtl-expand
# egypt *.expand > func.dot
#

if [ "$#" -lt 3 ]; then
	echo "Usage $0 ctags dot output-file"
	echo ""
	echo "ctags -x *.h *.c >func.tag"
	echo "make clean"
	echo "make CFLAGS+=-fdump-rtl-expand"
	echo "egypt *.expand >func.dot"
	echo "$0 func.tag func.dot OUTPUT-FILENAME"
	echo ""
	echo "options:"
	echo "  -D    DEBUG"
	echo "  -F    function only"
	echo "  -E    not external"
	exit 0
fi

TAG=$1
DOT=$2
OUT=$3

DEBUG="N"
FUNCTION_ONLY="N"
NO_EXTERN="N"

for i in `seq 4 $#`
do
	if [ "$4" = "-D" ]; then
		DEBUG="Y"
	fi
	if [ "$4" = "-F" ]; then
		FUNCTION_ONLY="Y"	
	fi
	if [ "$4" = "-E" ]; then
		NO_EXTERN="Y"
	fi
	shift
done

#echo "${TAG},${DOT},${OUT},${DEBUG},${FUNCTION_ONLY}"
#exit 0

TMP1=./$0.tmp1
TMP2=./$0.tmp2

ID_FILE=./$0.tmpID
TYPE_FILE=./$0.tmpTYPE
FILE_FILE=./$0.tmpFILE
CONT_FILE=./$0.tmpCONT
FILE_UNIQ=./$0.tmpUNIQ
EXTERN_FILE=./$0.tmpExt

GLOBAL=./$0.tmpGLOBAL
LINK_LABEL=./$0.tmpLINK_LABEL
LINK=./$0.tmpLINK
LABEL=./$0.tmpLABEL
CARD=./$0.tmpCARD
CARD_DATA=./$0.tmpCARD_DATA

echo "[Global]\n\
Version=7\n\
Arrange=1\n\
ArrangeMode=3\n\
AutoScroll=0\n\
AutoZoom=0\n\
FullScreen=-1\n\
Exit=-1\n\
Zoom=0\n\
X=0.5\n\
Y=0.5\n\
TargetCard=-1000\n\
SizeLimitation=0\n\
LinkLimitation=0\n\
DateLimitation=0\n\
SizeLimitation=100\n\
LinkLimitation=3\n\
LinkDirection=0\n\
LinkBackward=0\n\
LinkTarget=0\n\
DateLimitation=0\n\
DateLimitationDateType=0\n\
DateLimitationType=0" >${GLOBAL}

# filter TAG file
if [ "${FUNCTION_ONLY}" = "Y" ]; then
	grep " function " ${TAG} >${TMP1}
else
	cp ${TAG} ${TMP1}
fi

# separates info(ID/TYPE/FILE/Contents) to each file
cat ${TMP1} | awk '{ print $1 }' >${ID_FILE}
cat ${TMP1} | awk '{ print $2 }' >${TYPE_FILE}
cat ${TMP1} | awk '{ print $4 }' >${FILE_FILE}
cat ${TMP1} | awk '{for(i=5; i<NF; i++) { printf "%s ", $i } printf "\n" }' >${CONT_FILE}

lineAt() {
	AT=`expr $2 + 1`
	sed -n -e "${AT}p" <$1
}

NoOf() {
	FILE=$1
	STR=$2
	NO=0
	while read LINE
	do
		if [ "$LINE" = "$STR" ]; then
			echo "${NO}"
			return 0
		fi
		NO=`expr ${NO} + 1`
	done <${FILE}
	echo "-1"
}

fileLabelNo() {
	FILE=`lineAt ${FILE_FILE} $1`
	NO=`NoOf ${FILE_UNIQ} ${FILE}`
	echo "${NO}"
}

typeLabelNo() {
	TYPE=`lineAt ${TYPE_FILE} $1`
	if [ "${TYPE}" = "macro" ]; then
		echo "0"
	elif [ "${TYPE}" = "function" ]; then
		echo "1"
	elif [ "${TYPE}" = "variable" ]; then
		echo "2"
	elif [ "${TYPE}" = "member" ]; then
		echo "3"
	elif [ "${TYPE}" = "typedef" ]; then
		echo "4"
	elif [ "${TYPE}" = "enumerator" ]; then
		echo "5"
	elif [ "${TYPE}" = "struct" ]; then
		echo "6"
	else
		echo "<typeLabelNo():'${TYPE}' at $1"
		exit 0
	fi
}

echo "[LinkLabel]\nNum=2\n\
0=Co16711680,En1,Sh1,Hi0,Fo0,Si100,NaCall\n\
1=Co65280,En1,Sh1,Hi0,Fo0,Si100,NaRefer" >${LINK_LABEL}

sort ${FILE_FILE} | uniq >${FILE_UNIQ}
LABEL_NO=`cat ${FILE_UNIQ} | wc -l`
LABEL_NO8=`expr ${LABEL_NO} + 8`

echo "[Label]\nNum=${LABEL_NO8}\n\
0=Co16774400,En0,Sh0,Hi0,Fo0,Si100,Namacro\n\
1=Co21247,En0,Sh0,Hi0,Fo0,Si100,Nafunction\n\
2=Co65357,En0,Sh0,Hi0,Fo0,Si100,Navariable\n\
3=Co5322,En0,Sh0,Hi0,Fo0,Si100,Namember\n\
4=Co1232,En0,Sh0,Hi0,Fo0,Si100,Natypedef\n\
5=Co9222,En0,Sh0,Hi0,Fo0,Si100,Naenumerator\n\
6=Co6211,En0,Sh0,Hi0,Fo0,Si100,Nastruct\n\
7=Co3211,En0,Sh0,Hi0,Fo0,Si100,NaExternal\n\
" >${LABEL}
NO=0
while read LINE
do
	LNO=`expr ${NO} + 8`
	echo "${LNO}=Co${LNO}323,En1,Sh1,Hi0,Fo0,Si100,Na${LINE}" >>${LABEL}
	NO=`expr ${NO} + 1`
done <${FILE_UNIQ}

echo "[CardData]" >${CARD_DATA}
DATE=`date +"%Y/%m/%d %H:%M:%S"`
CARD_NO=0
while read ID 
do
	FILE_NAME=`lineAt ${FILE_FILE} ${CARD_NO}`
	FILE_LABEL_NO=`NoOf ${FILE_UNIQ} ${FILE_NAME}`
	###echo "ID:{$ID},FILE_NAME:${FILE_NAME},FILE_LABEL_NO:${FILE_LABEL_NO}"
	FILE_LABEL_NO3=`expr ${FILE_LABEL_NO} + 9`
	TYPE_LABEL_NO=`typeLabelNo ${CARD_NO}`
	TYPE_LABEL_NO=`expr ${TYPE_LABEL_NO} + 1`
	CARD_NO=`expr ${CARD_NO} + 1`
	echo "13\n\
Title:${ID}\n\
Label:${FILE_LABEL_NO3},${TYPE_LABEL_NO}\n\
Fixed:0\n\
X:0.5\n\
Y:0.5\n\
Size:100\n\
Shape:2\n\
Visible:1\n\
Created:${DATE}\n\
Updated:${DATE}\n\
Viewed:${DATE}\n-" >>${CARD_DATA}
	sed -n -e "${CARD_NO}p" ${CONT_FILE} >>${CARD_DATA}
done <${ID_FILE}	


echo -n "" >${EXTERN_FILE}

addCardData() {
	echo "12\n\
Title:${1}\n\
Label:8\n\
Fixed:0\n\
X:0.5\n\
Y:0.5\n\
Size:100\n\
Shape:3\n\
Visible:1\n\
Created:${DATE}\n\
Updated:${DATE}\n\
Viewed:${DATE}\n-" >>${CARD_DATA}

echo "$1" >>${EXTERN_FILE}
}

echo -n "" >${LINK}
LINK_NO=0
ID_NO=0
while read ID
do
	echo "Search Links of ${ID}[${ID_NO}]"
	grep -w ^\"${ID}\" ${DOT} >${TMP2}
	if [ $? -eq 0 ]; then
		LINES=`cat ${TMP2} | wc -l`
		REF=0
		echo "\tfound ${LINES} link(s)"
		while [ ${REF} -lt ${LINES} ]
		do
			LINE=`lineAt ${TMP2} ${REF}`
			FUNC=`expr "$LINE" : ".*-> \"\(.*\)\".*"`
			if [ ! "${FUNC}" = "" ]; then
				NO=`NoOf ${ID_FILE} ${FUNC}`
				if [ ! ${NO} -eq -1 ]; then
					TYPE=`typeLabelNo ${NO}`
					echo "\trefers ${FUNC} of ID:${NO}, TYPE:${TYPE}"
					echo "${LINK_NO}=Fr${ID_NO},De${NO},Di1,Sh0,La${TYPE},Na" >>${LINK}
					LINK_NO=`expr ${LINK_NO} + 1`
				else
					if [ "${NO_EXTERN}" = "Y" ]; then
						echo "\trefers ${FUNC} but no tag entried"
					else
						EXT_ID=`NoOf ${EXTERN_FILE} ${FUNC}`
						if [ ${EXT_ID} -eq -1 ]; then
							addCardData ${FUNC}
							EXT_ID=${CARD_NO}
							CARD_NO=`expr ${CARD_NO} + 1`
						fi
						echo "${LINK_NO}=Fr${ID_NO},De${EXT_ID},Di1,Sh0,La1,Na" >>${LINK}
						LINK_NO=`expr ${LINK_NO} + 1`
					fi
				fi
			fi
			REF=`expr ${REF} + 1`
		done
	fi
	ID_NO=`expr ${ID_NO} + 1`
done <${ID_FILE}

#CARD_NO=`cat ${ID_FILE} | wc -l`
#echo "CARD_NO:${CARD_NO}"
echo "[Card]\nCardId=${CARD_NO}\nNum=${CARD_NO}" >${CARD}
NO=0
while [ ${NO} -lt ${CARD_NO} ]
do
	echo "${NO}=${NO}" >>${CARD}
	NO=`expr ${NO} + 1`
done

while read LINE
do
	echo "${NO}=${NO}" >>${CARD}
	NO=`expr ${NO} + 1`
done <${EXTERN_FILE}

NO=`cat ${LINK} | wc -l`
cat ${GLOBAL} >${OUT}
cat ${CARD} >>${OUT}
echo "[Link]\nNum=${NO}" >>${OUT}
cat ${LINK} >>${OUT}
cat ${LABEL} >>${OUT}
cat ${LINK_LABEL} >>${OUT}
cat ${CARD_DATA} >>${OUT}

if [ "${DEBUG}" = "N" ]; then
	rm -rf $0.tmp*
fi

