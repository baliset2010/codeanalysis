#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""C#のソリューションをオブジェクト化する

バグ：ソースが見つからないエラーが発生したらシンボリック・リンクしてみる
"""

import sys
import os
import io
import codecs

gOptionFileEncoding='utf-8'

DEBUG = False

def unCommentFile(path):
    """ C#のソースをパースしてコメントを外すして各行をリストにして返す
    """
    src = []
    inComment = False
    print(path)
    for line in codecs.open(path, 'r', encoding=gOptionFileEncoding):
        pos = line.find('//')
        if pos >= 0:
            line = line[:pos]
        pos = line.find('/*')
        if pos >= 0:
            if line.find('*/'):
                line = line[:pos] + line[(line.find('*/') + 2):]
            else:
               inComment = True
        if pos >= 0:
            if line.find('*/'):
                if inComment:
                    inComment = False
        if inComment == False:
            src.append(line)
    return src

def parsePath(path):
    """ Windows のパスを修正する
    """
    path = path.replace('\\', '/')
    return path

class CSharpClass:
    """ C#のクラス
    """
    instances = []
    
    def __init__(self, name, basename, namespace):
        """ 初期化

            name - クラス名
            basename - ベースクラス
            namespace - ネームスペース
        """
        self.name = name
        self.base = basename
        self.namespace = namespace
        self.funcs = []

    @classmethod
    def getInstance(cls, name, basename, namespace):
        """ インスタンスを返す

            name - クラス名
            basename - ベースクラス
            namespace - ネームスペース

            同じクラスが既に登録されている場合は(partial class のため)、それを返す。
        """
        for ins in cls.instances:
            if ins.name == name and ins.namespace == namespace:
                return ins
        ins = CSharpClass(name, basename, namespace)
        cls.instances.append(ins)
        return ins
    
    def addFunc(self, funcName):
        """ 関数登録

            funcName : 関数名
        """
        self.funcs.append(funcName)

    
class CSharp:
    """ Ｃ＃のモジュール・オブジェクト
    """
    
    _CLASS = 'class '
    _PUBLIC_CLASS = 'public class '
    _PUBLIC_PARTIAL_CLASS = 'public partial class '
    _PUBLIC_ABSTRACT_CLASS = 'public abstract class '
    _PUBLIC = 'public '
    _NAMESPACE = 'namespace '
    
    def __init__(self, path):
        """ 初期化

            path : ソースのパス
        """
        self.path = parsePath(path)
        self.classes = []
        self._readCSharp()
        
    def _readCSharp(self):
        """ Ｃ＃のソースを読む

            C#のソースを読み込み、クラス、それに属するネーム・スペース、関数を登録してクラスのコレクションを返す
        """
        if DEBUG:
            print(self.path)
        for line in unCommentFile(self.path):
            _class = self._CLASS in line
            _publicClass = self._PUBLIC_CLASS in line
            _publicPartialClass = self._PUBLIC_PARTIAL_CLASS in line
            _publicAbstractClass = self._PUBLIC_ABSTRACT_CLASS in line

            if self._NAMESPACE in line:
                pos = line.find(self._NAMESPACE) + len(self._NAMESPACE)
                namespace = line[pos:].strip(' \r\n{')                
                    
            elif _class or _publicClass or _publicPartialClass or _publicAbstractClass:
                if _publicClass:
                    pos = line.find(self._PUBLIC_CLASS) + len(self._PUBLIC_CLASS)
                elif _publicPartialClass:
                    pos = line.find(self._PUBLIC_PARTIAL_CLASS) + len(self._PUBLIC_PARTIAL_CLASS)
                elif _class:
                    pos = line.find(self._CLASS) + len(self._CLASS)
                else:
                    pos = line.find(self._PUBLIC_ABSTRACT_CLASS) + len(self._PUBLIC_ABSTRACT_CLASS)
                        
                # class definition
                classname = line[pos:].strip(' \r\n{')
                epos = classname.find(' ')
                if epos >= 0:
                    classname = classname[:epos]
                classname = classname.strip('"')

                if line.find(':') >= 0:
                    ll = line[(line.find(':') + 1):].strip()
                    if ll.find(',') >= 0:
                        ll = ll[:ll.find(',')]
                    basename = ll
                else:
                    basename = 'Object'
                basename = basename.strip('" {')
                if DEBUG:
                    print('find class ' + classname + ':' + basename)
                    print('\t:' + line)
                currentClass = CSharpClass.getInstance(classname, basename, namespace)
                self.classes.append(currentClass)
                
                    
            elif self._PUBLIC in line and '(' in line and ')' in line:
                if ' delegate ' in line:
                    pass  # delegate
                else:
                    pos = line.find(':')
                    if pos >= 0:
                        if line[pos+1] != ':':
                            lll = line[:pos]
                        else:
                            lll = line
                    else:
                        lll = line
                    # public method definition
                    ll = lll.split(' ')
                    funcName = ''
                    for l in ll:
                        if '(' in l:
                            funcName = l
                        elif len(funcName) > 0:
                            if (')' in funcName) == False:
                                funcName += l
                    funcName = funcName.strip(' \r\n{')
                    if DEBUG:
                        print('\tfind func:' + funcName)
                        print('\t\t:' + line)
                    currentClass.addFunc(funcName)
                
class Project:
    """ Ｃ＃のプロジェクト・オブジェクト
    """
    def __init__(self, baseDirPath, dirPath, projectName):
        """ 初期化

            baseDirPath : ベースのディレクトリ
            dirPath : プロジェクトのディレクトリ
            projectName : プロジェクト名
        """
        self.basePath = baseDirPath
        self.dirPath = os.path.join(baseDirPath, dirPath)
        self.path = os.path.join(baseDirPath, projectName)
        self.name = projectName
        self.refs = []
        self.projectRefs = []
        self.sources = []
        self._readProject()

    def _readProject(self):
        """ プロジェクトを読み組み、Ｃ＃オブジェクトをコレクションにする
        """
        for line in open(self.path, 'r', encoding='utf-8'):
            l = line.strip()
            if l.startswith('<AssemblyName>'):
                spos = l.find('>')
                epos = l.rfind('<')
                _assemblyName = l[(spos+1):epos]
                self.assemblyName = _assemblyName
            elif l.startswith('<Reference Include="'):
                spos = l.find('"')
                epos = l.rfind('"')
                _ref = (l[(spos+1):epos].split(','))[0]
                self.refs.append(_ref)
            elif l.startswith('<ProjectReference Include="'):
                spos = l.find('"')
                epos = l.rfind('"')
                _pref = l[(spos+1):epos]
                self.projectRefs.append(_pref)
            elif l.startswith('<Compile Include="'):
                spos = l.find('"')
                epos = l.rfind('"')
                cinc = l[(spos+1):epos]
                csharp = CSharp(os.path.join(self.dirPath, cinc))
                self.sources.append(csharp)

    def _debugPrint(self):
        """ デバッグ出力
        """
        print("\t" + self.name)
        print('\trefs(' + str(len(self.refs)) + ')  ' + self.assemblyName)
        for ref in self.refs:
            print('\t\t' + ref)
        print('\tref projects(' + str(len(self.projectRefs)) + ')')
        for proj in self.projectRefs:
            print('\t\t' + proj)
        print('\tsrc(' + str(len(self.sources)) + ')')
        for src in self.sources:
            print('\t\tS ' + src.path)
            for cls in src.classes:
                print('\t\t\tC ' + cls.name + ':' + cls.base)
                for func in cls.funcs:
                    print('\t\t\t\tF ' + func)
        
class Solution:
    """ ソリューション・オブジェクト
    """
    def __init__(self, path):
        """ 初期化

            path : ソリューションのパス
        """
        self.path = parsePath(path)
        self.dirPath = os.path.dirname(self.path)
        self.projects = []
        self._readSolution()
        
    def _readSolution(self):
        """ ソリューション読み込み
        """
        for line in open(self.path, 'r', encoding='utf-8'):
            l = line.strip()
            if l.startswith('Project("'):
                ll = l[(l.find('=') + 1):].split(',')
                ll[0] = ll[0].strip().strip('"')
                ll[1] = ll[1].strip().strip('"')
                #print("0 : '" + ll[0] + "'")
                #print("1 : '" + ll[1] + "'")
                #print("path:" + self.dirPath)
                ll[0] = ll[0].replace('\\', '/')
                ll[1] = ll[1].replace('\\', '/')
                if ll[1].endswith('.csproj'):
                    self.projects.append(Project(self.dirPath, ll[0], ll[1]))
                
    def _debugPrint(self):
        """ デバッグ出力
        """
        print(self.path)
        print(str(len(self.projects)) + " projects")
        for proj in self.projects:
            proj._debugPrint()






def printNumber(solution):
    print("solution has " + str(len(solution.projects)) + " project(s)")
    for project in solution.projects:
        print("\tproject;" + project.name + " has " + str(len(project.sources)) + " source(s)")
        for src in project.sources:
            print("\t\tC# source;" + os.path.basename(src.path) + " has " + str(len(src.classes)) + " class(es)")
            for cls in src.classes:
                print("\t\t\tclass;" + cls.name + " has " + str(len(cls.funcs)) + " function(s)")

def printSource(solution):
    for project in solution.projects:
        for src in project.sources:
            print(os.path.basename(src.path) + " in " + os.path.basename(project.name) +
                  " has " + str(len(src.classes)) + " class(es)")
    

def printClass(solution):
    for project in solution.projects:
        for src in project.sources:
            for cls in src.classes:
                print(cls.name + " in " + os.path.basename(src.path) + " in " +
                      os.path.basename(project.name) + " has " + str(len(cls.funcs)) + " function(s)")

def printClassNameOnly(solution):
    for project in solution.projects:
        for src in project.sources:
            for cls in src.classes:
                print(cls.name)


def printFunc(solution):
    for project in solution.projects:
        for src in project.sources:
            for cls in src.classes:
                for func in cls.funcs:
                    print(func + " in " + cls.name + " in " + os.path.basename(src.path) +
                          " in " + os.path.basename(project.name))

def printFuncNameOnly(solution):
    for project in solution.projects:
        for src in project.sources:
            for cls in src.classes:
                for func in cls.funcs:
                    print(func)
    
                    
if __name__ == '__main__':

    proc = 'print'
    #proc = 'funcNameOnly'
    solutionPath = None

    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]
        if arg == '-p':
            # print solution structure
            proc = 'print'
        elif arg == '-S':
            # specify solution path
            solutionPath = sys.argv[i+1]
            i = i + 1
        elif arg.startswith('-S'):
            # specify solutino path
            solutionPath = sys.argv[2:]
        elif arg == '-d':
            # set DEBUG print on
            DEBUG = True
        elif arg == '-n':
            # print numbers of projects/sources/classes/functions
            proc = 'number'
        elif arg == '-s':
            # list up sources
            proc = 'source'
        elif arg == '-c':
            # list up classes
            proc = 'class'
        elif arg == '-C':
            # list up class names only
            proc = 'classNameOnly'
        elif arg == '-f':
            # list up functions
            proc = 'func'
        elif arg == '-F':
            # list up functions only
            proc = 'funcNameOnly'
        elif arg == '-e':
            gOptionFileEncoding = sys.argv[i+1]
            i = i + 1
        elif arg.startswith('-e'):
            gOptionFileEncoding = arg[2:]
        else:
            print('Usage:' + sys.argv[0] + ' -S PATH options...')
            print('  -S PATH : specify solution path')
            print('  -p  : print solution structure')
            print('  -n  : print number of functions')
            print('  -s  : list up sources')
            print('  -c  : list up classes')
            print('  -C  : class name only')
            print('  -f  : list up functions')
            print('  -F  : function only')
            print('  -e ENCODING : change encoding from utf-8 to something like, shift_jis')
            print('  -d  : debug print')
            sys.exit(0)

        i = i + 1

    if solutionPath is None:
        print('specify solution path using "-S"') 
        sys.exit(1)

    solution = Solution(solutionPath)

    if proc == 'print':
        solution._debugPrint()
    elif proc == 'number':
        printNumber(solution)
    elif proc == 'source':
        printSource(solution)
    elif proc == 'class':
        printClass(solution)
    elif proc == 'classNameOnly':
        printClassNameOnly(solution)
    elif proc == 'func':
        printFunc(solution)
    elif proc == 'funcNameOnly':
        printFuncNameOnly(solution)
        
        
        
        
        
