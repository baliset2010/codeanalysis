#!/usr/bin/env python3

import sys
import frieve

gOptionOmitDirectoryPath = False

class CFunc:
    def __init__(self, name, level, define, fileName, lineNo):
        self.name = name
        self.level = level
        self.define = define
        self.fileName = fileName
        self.lineNo = lineNo
        self.parents = []
        self.children = []

    def addParent(self, func):
        if func.name not in [ f.name for f in self.parents]:
            self.parents.append(func)
            func.addChild(self)

    def addChild(self, func):
        if func.name not in [ f.name for f in self.children]:
            self.children.append(func)
            func.addParent(self)

    def printString(self):
        mark = ' +- ' if self.level > 0 else ''
        print('    ' * self.level + mark + self.name)
        for f in self.children:
            f.printString()

def getFuncInfo(line):
    n = len(line)
    for i in range(len(line)):
        if line[i] != ' ':
            n = i
            break
    level = n // 4
    funcName = line.strip().split(' ')[0]
    funcName = funcName.split('(')[0]
    if line.find('<') >= 0:
        spos = line.find('<') + 1
        epos = line.find('>', spos)
        s = line[spos:epos]
        l = s.split(' at ')
        define = l[0]
        l = l[1].split(':')
        fileName = l[0]
        if gOptionOmitDirectoryPath:
            fileName = fileName.split('/')
            fileName = fileName[len(fileName) - 1]
        lineNo = l[1]
    else:
        define = None
        fileName = None
        lineNo = None
    return (funcName, level, define, fileName, lineNo)

def addFuncCard(fe, f):
    card = fe.addCard(f.name)
    if f.fileName is not None:
        label = fe.getLabelId(f.fileName)
        if label == -1:
            label = fe.addLabel(f.fileName)
        print(f.name + ' - ' + f.fileName + ' label:' + str(label))
        fe.addLabelToCard(label + 1, card)  # BUG OF frieve.py ?????
    else:
        print(f.name + ' NO FILENAME')
    for child in f.children:
        addFuncCard(fe, child)

def addLinkCard(fe, f):
    parent = fe.getCardId(f.name)
    for child in f.children:
        childId = fe.getCardId(child.name)
        fe.addLink(parent, childId)
        addLinkCard(fe, child)

def CreateFunctionGraph(funcs, fileName):
    fe = frieve.Frieve()
    label = fe.addLabel('func')
    linkLabel = fe.addLinkLabel('call')

    for f in funcs:
        addFuncCard(fe, f)

    for f in funcs:
        addLinkCard(fe, f)

    fp = open(fileName + '.fip', 'w', encoding='utf-8')
    fe.write(fp)

if __name__ == '__main__':
    i = 1
    mainFunction = None
    matchedFunc = None
    printOnly = False

    while i < len(sys.argv):
        arg = sys.argv[i]
        if arg == '-f':
            mainFunction = sys.argv[i+1]
            i = i + 1
        elif arg == '-p':
            printOnly = True
        elif arg == '-d':
            gOptionOmitDirectoryPath = True
        else:
            print("*cflow2frieveeditor*")
            print(" create a file for Frieve Editor to show function call grah")
            print(" Usage: cflow -b [C source files] | ./cflow2frieveeditor.py [-f ROOT_FUNC_NAME] [-p] [-d]")
            print("   Options")
            print("     -f ROOT_FUNCTION_NAME")
            print("     -p : print function status only")
            print("     -d : omit directory path in card attribution")
            sys.exit(0)

        i = i + 1

    baseFuncs = []
    funcs = []
    lastFunc = [None] * 100             # doesn't support recursive, now!
    for line in sys.stdin.readlines():
        func, level, define, fileName, lineNo = getFuncInfo(line)
        func = CFunc(func, level, define, fileName, lineNo)
        if level == 0:
            baseFuncs.append(func)
        else:
            lastFunc[level - 1].addChild(func)
        lastFunc[level] = func
        if func.name not in [f.name for f in funcs]:
            funcs.append(func)
        if mainFunction is not None:
            if mainFunction == func.name:
                matchedFunc = func

    if mainFunction is not None:
        if matchedFunc is None:
            print('main function:' + mainFunction + ' was not found')
            sys.exit(1)

    if printOnly:
        for f in funcs:
            f.printString()
            print('number of func:' + str(len(funcs)))
    else:
        if mainFunction is None:
            CreateFunctionGraph(funcs, 'all')
        else:
            CreateFunctionGraph([matchedFunc], mainFunction)
