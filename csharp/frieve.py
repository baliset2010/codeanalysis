#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Frieve Editor のデータをオブジェクト化し書き出す。
　　読み込みはできない
"""

import os
import sys
import io

class Frieve():
    """FrieveEditor のデータ・オブジェクト
    """
    def __init__(self):
        """初期化
        """
        self.initializeGlobal()
        self.initializeCard()
        self.initializeLink()
        self.initializeLabel()
        self.initializeLinkLabel()
        self.initializeCardData()

    def write(self, fp):
        """データをデータ・ファイルに書き出す
        """
        self.writeGlobal(fp)
        self.writeCard(fp)
        self.writeLink(fp)
        self.writeLabel(fp)
        self.writeLinkLabel(fp)
        self.writeCardData(fp)
        
    def initializeGlobal(self):
        """Globalセクションの設定
        """
        self._global = 'Version=7\n' \
                       'Arrange=0\n' \
                       'ArrangeMode=2\n' \
                       'AutoScroll=1\n' \
                       'AutoZoom=0\n' \
                       'FullScreen=-1\n' \
                       'Exit=-1\n' \
                       'Zoom=-1\n' \
                       'X=0.5\n' \
                       'Y=0.5\n' \
                       'TargetCard=-1000\n' \
                       'SizeLimitation=0\n' \
                       'LinkLimitation=0\n' \
                       'DateLimitation=0\n' \
                       'SizeLimitation=100\n' \
                       'LinkLimitation=3\n' \
                       'LinkDirection=0\n' \
                       'LinkBackward=0\n' \
                       'LinkTarget=-1\n' \
                       'DateLimitationDateType=0\n' \
                       'DateLimitationType=0\n'
        
    def writeGlobal(self, fp):
        fp.write('[Global]\n')
        fp.write(self._global)
        
    def initializeCard(self):
        self._card = []

    def writeCard(self, fp):
        fp.write('[Card]\nCardID=-1\r\n')
        fp.write('Num=' + str(len(self._card)) + '\n')
        for i in range(len(self._card)):
            fp.write(str(i) + '=' + str(i) + '\n')

    def initializeLink(self):
        self._link = []

    def writeLink(self, fp):
        fp.write('[Link]\n')
        fp.write('Num=' + str(len(self._link)) + '\n')
        for i in range(len(self._link)):
            link = self._link[i]
            cardFrom = link[0]
            cardTo = link[1]
            linkLabels = link[2]
            fp.write(str(i) + '=Fr' + str(cardFrom) + ',De' + str(cardTo) +
                     ',Di1,Sh0,')
            for j in range(len(linkLabels)):
                fp.write('La' + str(linkLabels[j]) + ',')
            fp.write('Na\n')
            
    def initializeLabel(self):
        self._label = []

    def writeLabel(self, fp):
        fp.write('[Label]\n')
        fp.write('Num=' + str(len(self._label)) + '\n')
        for i in range(len(self._label)):
            label = self._label[i]
            fp.write(str(i) + '=Co' + str(label[1])
                     + ',En1,Sh1,Hi0,Fo0,Si100,Na' + label[0] + '\n')

    def initializeLinkLabel(self):
        self._linkLabel = []

    def writeLinkLabel(self, fp):
        fp.write('[LinkLabel]\n')
        fp.write('Num=' + str(len(self._linkLabel)) + '\n')
        for i in range(len(self._linkLabel)):
            label = self._linkLabel[i]
            fp.write(str(i) + '=Co' + str(label[1])
                     + ',En1,Sh1,Hi0,Fo0,Si100,Na' +
                     label[0] + '\n')

    def initializeCardData(self):
        self._cardData = []

    def writeCardData(self, fp):
        fp.write('[CardData]\n')
        for i in range(len(self._cardData)):
            cardData = self._cardData[i]
            if len(cardData['Label']) == 0:
                fp.write('11\n')
            else:
                fp.write('12\n')
            for key in cardData:
                value = cardData[key]
                if isinstance(value, list):
                    fp.write(key + ':')
                    for j in range(len(value)):
                        fp.write(str(value[j]))
                        if j == len(value) - 1:
                            fp.write('\n')
                        else:
                            fp.write(',')
                else:
                    fp.write(key + ':' + str(cardData[key]) + '\n')
            fp.write('-\n')


    def addCard(self, name):
        cardId = self.getCardId(name)
        if cardId != -1:
            return cardId
        card = {}
        card['Title'] = name
        card['Label'] = []
        card['Fixed'] = 0
        card['X'] = 0.5
        card['Y'] = 0.5
        card['Size'] = 100
        card['Shape'] = 2
        card['Visible'] = 1
        card['Created'] = '2017/10/02 23:59:59'
        card['Updated'] = '2017/10/02 23:59:59'
        card['Viewed'] = '2017/10/02 23:59:59'

        idNo = len(self._card)
        self._card.append(card)
        self._cardData.append(card)
        return idNo

    def getCardId(self, name):
        for i in range(len(self._card)):
            if (self._card[i])['Title'] == name:
                return i
        return -1

    def addLink(self, cardId1, cardId2):
        linkId = self.getLinkId(cardId1, cardId2)
        if linkId != -1:
            return linkId
        link = [cardId1, cardId2, [] ]
        idNo = len(self._link)
        self._link.append(link)
        return idNo

    def getLinkId(self, cardId1, cardId2):
        for i in range(len(self._link)):
            l = self._link[i]
            if l[0] == cardId1 and l[1] == cardId2:
                return i
        return -1
    
    def addLabel(self, name, color=16711680):
        labelId = self.getLabelId(name)
        if labelId != -1:
            return labelId
        label = [ name, color ]
        idNo = len(self._label)
        self._label.append(label)
        return idNo

    def getLabelId(self, name):
        for i in range(len(self._label)):
            l = self._label[i]
            if l[0] == name:
                return i
        return -1
    
    def addLinkLabel(self, name, color=16724480):
        linkId = self.getLinkLabelId(name)
        if linkId != -1:
            return linkId
        label = [ name, color ]
        idNo = len(self._linkLabel)
        self._linkLabel.append(label)
        return idNo

    def getLinkLabelId(self, name):
        for i in range(len(self._linkLabel)):
            l = self._linkLabel[i]
            if l[0] == name:
                return i
        return -1

    def addLabelToCard(self, labelId, cardId):
        card = self._card[cardId]
        labels = card['Label']
        if labelId not in labels:
            labels.append(labelId)

    def addLabelToLink(self, linkId, linkLabelId):
        link = self._link[linkId]
        if linkLabelId not in link[2]:
            link[2].append(linkLabelId)    

        
if __name__ == "__main__":
    #sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    #sys.stdint = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')

    frieve = Frieve()
    
    card1 = frieve.addCard('card1')
    card2 = frieve.addCard('card2')
    card3 = frieve.addCard('card3')

    label1 = frieve.addLabel('label1', 16711680)
    label2 = frieve.addLabel('label2', 255)
    label3 = frieve.addLabel('label3', 16122111)

    linkLabel1 = frieve.addLinkLabel('linkLabel1', 16736768)

    link1 = frieve.addLink(card1, card2)
    link2 = frieve.addLink(card2, card3)
    link3 = frieve.addLink(card1, card3)

    frieve.addLabelToCard(label1, card1)
    frieve.addLabelToCard(label2, card1)
    frieve.addLabelToCard(label3, card2)

    frieve.addLabelToLink(linkLabel1, link2)

    fp = open('a.fip', 'w')
    frieve.write(fp)
    
