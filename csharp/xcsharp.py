#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""C#のソリューションをクロスリファレンスをとる
"""

import sys
import os
import io
import re

import csharp
import frieve

gOptionFileEncoding = 'utf-8'

ValidClasses = []

def getClassName(line, pos):
    xpos = line.find('class ', pos) + 6
    l = line[xpos:].split(' ')[0]
    if ':' in l:
        l = l.split(':')[0]
    l = l.strip()
    #print(l)
    return l

def CreateClassReference(clsName):
    print(clsName)
    clsRefs = []
    paths = []
    projs = []
    for project in solution.projects:
        for src in project.sources:
            path = src.path
            currentClass = ''
            for line in csharp.unCommentFile(path):
                if 'class ' in line:
                    pos = line.find('class ')
                    currentClass = getClassName(line, pos)
                #elif 'public class ' in line:
                #    currentClass = getClassName(line, pos)
                #elif 'public partial class ' in line:
                #    currentClass = getClassName(line, pos)
                #elif 'public sbstract class ' in line:
                #    currentClass = getClassName(line, pos)
                elif clsName in line:
                    if clsName != currentClass:
                        if currentClass in ValidClasses:
                            if currentClass not in clsRefs:
                                print("\t" + currentClass + ":" + path)
                                clsRefs.append(currentClass)
                                paths.append(os.path.basename(path))
                                projs.append(os.path.basename(project.name))
    return [ clsRefs, paths, projs ]



def readClassesFromFile(path):
    l = []
    for line in open(path, 'r', encoding=gOptionFileEncoding):
        l.append(line.rstrip('\r\n'))
    return l

def validateAllClasses(solution):
    for project in solution.projects:
        for src in project.sources:
            for cls in src.classes:
                ValidClasses.append(cls.name)
                
if __name__ == '__main__':
    
    classReferenceOnly = True
    solutionPath = None
    ignoreClassFile = ''
    validClassFile = ''
    
    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]
        if arg == '-s':
            solutionPath = sys.argv[i+1]
            i = i + 1
        elif arg.startswith('-s'):
            solutionPath = arg[2:]
        elif arg == '-c':
            classReferenceOnly = True
        elif arg == '-i':
            ignoreClassFile = sys.argv[i+1]
            i = i + 1
        elif arg.startswith('-i'):
            ignoreClassFile = arg[2:]
        elif arg == '-v':
            validClassFile = sys.argv[i+1]
            i = i + 1
        elif arg.startswith('-v'):
            validClassFile = arg[2:]
        else:
            print("Usage: " + sys.argv[0] + ' [-s PATH] [-c] [-i] [-v]')
            print("  -s PATH(solution path *.sln")
            print("  -c   : cross reference only")
            print("  -i   : ignore class file")
            print("  -v   : valid class file")
            sys.exit(0)

        i = i + 1

    if solutionPath is None:
        print("specify solution path by using '-s'") 
        sys.exit(1)

    solution = csharp.Solution(solutionPath)

    if validClassFile != '':
        ValidClasses = readClassesFromFile(validClassFile)
    elif ignoreClassFile != '':
        ignoreClasses = readClassesFromFile(ignoreClassFile)
        # TODO
        # ValidClasses = solution.allClasses - ignoreClasses
    else:
        validateAllClasses(solution)

    fe = frieve.Frieve()
    linkLabelId = fe.addLinkLabel('call')
    
    for project in solution.projects:
        projId = fe.addLabel(os.path.basename(project.name))
        for src in project.sources:
            path = src.path
            #srcId = fe.addLabel(os.path.basename(src.path))
            for cls in src.classes:
                clsName = cls.name

                if clsName in ValidClasses:
                    clsBase = cls.base
                    clsFunctions = cls.funcs

                    calledClassId = fe.addCard(clsName)
                    fe.addLabelToCard(projId, calledClassId)
                    #fe.addLabelToCard(srcId, calledClassId)
                    
                    if classReferenceOnly:
                        refs = CreateClassReference(clsName)
                        classes = refs[0]
                        paths = refs[1]
                        projs = refs[2]

                        for i in range(len(classes)):
                            callerClassId = fe.addCard(classes[i])
                            callerProjectId = fe.addLabel(os.path.basename(projs[i]))
                            #callerSrcId = fe.addLabel(os.path.basename(paths[i]))
                            fe.addLabelToCard(callerProjectId, callerClassId)
                            #fe.addLabelToCard(callerSrcId, callerClassId)
                            
                            linkId = fe.addLink(callerClassId, calledClassId)
                            # Do not uncomment below; Frieve Editor crashes
                            #fe.addLabelToLink(linkId, linkLabelId)
                    else:
                        pass
    solutionName = os.path.basename(solutionPath)
    solutionName = solutionName[:len(solutionName) - 4]
    fp = open(solutionName + '.fip', 'w', encoding='utf-8')
    fe.write(fp)
    
